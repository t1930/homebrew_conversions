 

ARCANE TRADITION

At 2nd level, a wizard gains the Arcane Tradition feature. The
following Bookman option is available to a wizard, in addition
to those normally offered.

BOOKMAN

Part cult, part school, part army, the Bookmen are the largest
faction of spellcasters in all of Aberranterra. They are easily
recognized by their trademark gas masks, tailored suits, and,
of course, leatherbound books. What few words they speak
are reserved for devastating incantations followed by hideous
laughter.

Male or female, when a protege joins the Bookmen, they
must surrender their name and can only be addressed as
Bookman. The only other distinction the Bookmen have are
their curiously clean suits which signify their rank among
other Bookmen. Black Suits are those who've just earned
their masks and a position among the Bookmen. Green Suits
are the learned Bookmen and war machines. Tan Suits lead
the Bookmen legions. And the greatest of all Bookmen, the
White Suits watch over entire Bookmen Burgs.

BOOKMAN FEATURES

Wizard Bookman
Level Feature Armor Augmentations

The Bookman Suit and
The Bookman Mask,

and Bookman Armor, 12 + Int 3
Bookman Magic

6th The Green Suit 13 + Int 5

10th The Tan Suit 14 + Int

14th The White Suit 15 + Int
 

THE BOOKMAN SUIT AND THE BOOKMAN MASK
At 2nd level, you achieve the status of Black Suit. You are
given a Bookman mask and a Bookman suit, which is, of
course, black. Both of these items act as catalysts for your
magical abilities as described in the Bookman Magic section
below and they are both bonded to you. You cannot be
disarmed of either item unless you are incapacitated If either
is on the same plane of existence, you can summon your mask
or suit as a bonus action on your turn, causing your mask to
instantly appear in your hand and your mask to form around
your body.

If either is destroyed, you can perform a ritual over the
course of 1 hour, which can be during a short rest. At the end
of the ritual, a new mask, suit, or both appear.

Ifa creature casts detect magic on either item, both exude
strong abjuration magic.

BooKMAN ARMOR

At 2nd level, while you are wearing your Bookman suit and
you are not wearing any armor or wielding a shield, your AC
equals 12+ your Intelligence modifier. Your base AC increases
by 1 when you reach the 6th level (13 + Int), 10th level (14 +
Int) and 14th level (15 + Int).

a ly

STRONG AND WEAK Macics

Bookmen practice the belief that there are both
"strong" and "weak" magics in the world. They
believe that spells that come from the schools of
abjuration, conjuration, evocation, and necromancy
are strong spells, whereas spells that come from the
schools of divination, enchantment, illusion, and
transmutation are weak.

a
BooKMAN MacIc
Also at 2nd level, you learn the secrets of Bookman magic,
which allows you to imbue your Bookman suit and Bookman
mask with magical abilities known as augmentations. You
gain the Adaptation augmentation and two more
augmentations of your choice, which are detailed in the
Bookman Augmentations section below. You gain two
additional augmentations of your choice at the 6th, 10th, and
14th level.

Additionally, when you gain a level in this class, you can
choose one of the augmentations you've gained and replace it
with another augmentation that you could gain at that level

THE GREEN SUIT

At 6th level, your Bookman suit takes on a forest green hue. In
addition, while you are wearing your Bookman suit, whenever
you are hit by a spell attack, you can use your reaction to
reduce the damage. When you do so, the damage you take
from the attack is reduced by 1d6. The damage you reduce
increases by 1d6 at 10th level (2d6), and again at 14th level
(3d6).

THE Tan Suir

At 10th level, your suit takes on a tan hue. While you are
wearing your Bookman suit, you can cast the levitate spell at
will, without expending a spell slot. You do not require spell
components when you cast it in this way.

THE WHITE SuIT

At 14th level, your suit becomes stark white. While you are
wearing your Bookman suit, you gain resistance to
bludgeoning, piercing, and slashing damage from nonmagical
weapons.

BOOKMAN AUGMENTATIONS

If an augmentation has prerequisites, you must meet them to
gain it. You can learn the augmentation at the same time that
you meet its prerequisites. a level prerequisite refers to your
level in this class.

ADAPTATION

While wearing your Bookman mask, you can breathe
normally in any environment, and you gain a +2 bonus on
saving throws made against harmful gases and vapors (such
as cloudkill and stinking cloud effects, inhaled poisons, and
the breath weapon of some dragons).

ARCANE DEFENSE
Prerequisite: 10th level

While wearing your Bookman suit, you can cast the shield
spell at will without expending a spell slot.

AMPHIBIOUS
While wearing your Bookman mask, you can breathe both air
and water.

ANIMAL TONGUE

While wearing your Bookman mask, you can cast the speak to
animals spell at will without expending a spell slot. You do not
require spell components when you cast the spell using this
augmentation.
APE FISTS

While you are wearing your Bookman suit, your unarmed
strikes deal bludgeoning damage equal to 1d4 + your Strength
modifier. When you gain the 6th level in this class, your
attacks are considered magical for the purpose of overcoming
resistance and immunity.

BEAR STRENGTH
Prerequisite: 6th level

While you are wearing your Bookman suit, your Strength
score is equal to your Intelligence score. In addition, your
carrying capacity (including maximum load and maximum lift)
is doubled and you have advantage on Strength checks made
to push, pull lift, or break objects.

BERSERKER
Prerequisite: Ape Fists augmentation, 6th level

While you are wearing your Bookman suit, you can attack
twice, instead of once, whenever you take the Attack action on
your turn, Both of your attacks must be made with your
unarmed strike.

CAMOUFLAGE
Prerequisite: 6th level

While wearing your Bookman suit, Wisdom (Perception)
checks made to see you are made at disadvantage.

Dark LENSES
You can see in darkness and in magical darkness out to 60 ft.
If you already have darkvision, the range extends to 60 ft.

DEEP POCKETS
Prerequisite: 6th level

The interior pocket of your Bookman suit jacket becomes an
extradimensional space. If functions similar to a bag of
holding.

DISGUISED
Prerequisite: 6th level

While wearing your Bookman suit, you can cast disguise self
at will and you do not use a spell slot whenever you cast this
spell using this augmentation.

Far-SIGHTED
While you are wearing your Bookman mask, you have
advantage on Wisdom (Perception) checks that rely on sight.

FLIGHT
Prerequisite: 14th level

While you are wearing your Bookman suit, you gain a flying
speed of 30 feet.

Hypnotic LENSES

While you are wearing your Bookman mask, you can cast
charm person without expending a spell slot. Once you cast
this spell using this augmentation, you can't do so again until
you complete a long rest.
IMMUNE
Prerequisite: Protected augmentation, 10th level

While you are wearing your Bookman suit, you gain immunity
to any damage type you are resistant to through the Resistant
augmentation.

NEAR-SIGHTED

While wearing your Bookman mask, you have advantage on
Intelligence (Investigation) checks that rely on sight as long as
the subject of your investigation is within 5 feet of you.

NOvuRISHING THREADS
Prerequisite: 6th level

While wearing your Bookman suit, you do not require food or
water.

POLYGLOT
Prerequisite: 10th level

While wearing your Bookman suit, you can cast the tongues
spell at will without expending a spell slot. You do not require
spell components when you cast the spell using this
augmentation.

PROTECTED

While wearing your Bookman suit you gain a +1 bonus to your
Strength, Dexterity and Constitution saving throws. And while
you are wearing your Bookman mask, you gain a +1 bonus to
your Intelligence, Wisdom, and Charisma saving throws.

PsycHIc SHIELDING
Prerequisite: Shielded augmentation, 14th level

While wearing your Bookman mask, you gain resistance to
psychic damage as well as any effect that would sense your
emotions or read your thoughts, divination spells, and the
charmed condition.

QUIETED

While wearing your Bookman suit, your steps make no sound.
You have advantage on Dexterity (Stealth) checks that rely on
moving silently.

 
RESISTANT
Prerequisite: 6th level

Choose a damage type: acid, cold fire, lightning, necrotic,
poison, radiant, or thunder. While you are wearing your
Bookman suit, you are resistant to the type of damage you
chose. You can choose this augmentation multiple times, and
select a new damage type each time you do.

REVEALING LENSES
Prerequisite: 10th level

While wearing your Bookman mask, invisible creatures and
objects are visible to you.

SHIELDED
Prerequisite: 6th level

While wearing your Bookman mask, if a creature tries to
target you with divination, the target must make a spellcasting
check contested against your own spellcasting check. If the
target succeeds, it detects you as normal If the target fails, it
can't target you with divination magic for 24 hours.

TELEPATHIC
Prerequisite: 6th level

While wearing your Bookman mask, you can cast detect
thoughts without using a spell slot. Once you use this
augmentation to cast this spell, you can't do so again until you
complete a long rest.

ToucH
While wearing your Bookman suit, your hit point maximum
increases by 2 for each level you have in this class.

